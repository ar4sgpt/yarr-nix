{
  description = "yet another rss reader";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    yarr-src = {
      url = "github:nkanaev/yarr";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, yarr-src }:
    let
      allSystems = [
        "x86_64-linux"
        "aarch64-linux"
      ];

      yarrPkg = { lib, stdenv, gnumake, go, git }: stdenv.mkDerivation {
        name = "yarr";
        src = lib.cleanSource yarr-src;

        buildInputs = [ go ];
        nativeBuildInputs = [ gnumake git ];
        buildPhase = ''
          GOCACHE=$TMPDIR make GITHASH="${yarr-src.shortRev}"
        '';

        installPhase = ''
          mkdir -p $out/bin
          cp _output/yarr $out/bin/
        '';
      };

      # Helper to provide system-specific attributes
      forAllSystems = f: nixpkgs.lib.genAttrs allSystems (system: f {
        pkgs = import nixpkgs { inherit system; };
      });
    in
    {
      overlays.default = final: prev: {
        yarr = prev.callPackage yarrPkg {};
      };

      packages = forAllSystems ({ pkgs }: {
        default = self.packages.${pkgs.system}.yarr;
        yarr = pkgs.callPackage yarrPkg {};
      });

      nixosModules.default = import ./module.nix;
    };
}
